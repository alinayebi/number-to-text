package com.example.ali.numtotext;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final String[] unity = {"zero", "one ", "two ", "three ", "four ", "five ", "six ", "seven ", "eight ", "nine ", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        final String[] tens = {"", "", "twenty ", "thirty ", "fourty ", "fifty ", "sixty ", "seventy ", "eighty ", "ninety "};
        //final String[] tentonineteen = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        final String[] hundreds = {"", "one hundred ", "two hundred ", "three hundred ", "four hundred ", "five hundred ", "six hundred ", "seven hundred ", "eight hundred ", "nine hundred "};


        Button btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editText = (EditText) findViewById(R.id.txt1);


                int index = Integer.parseInt(editText.getText().toString());
                int firstpart = (int) Math.ceil(index / 100);
                int temp = index % 100;
                int secondpart = (int) Math.ceil(temp / 10);
                int thirdpart = temp % 10;


                if (temp<20&& temp!=0){
                    Toast.makeText(MainActivity.this, hundreds[firstpart]  + unity[temp], Toast.LENGTH_SHORT).show();
                }else if(thirdpart == 0 && secondpart != 0){
                    Toast.makeText(MainActivity.this, hundreds[firstpart] + tens[secondpart], Toast.LENGTH_SHORT).show();
                }else if((temp<20&& temp==0)){
                    Toast.makeText(MainActivity.this, hundreds[firstpart], Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(MainActivity.this, hundreds[firstpart] + tens[secondpart] + unity[thirdpart], Toast.LENGTH_SHORT).show();
                }



                /*if (thirdpart == 0 && secondpart != 0) {
                    Toast.makeText(MainActivity.this, hundreds[firstpart] + tens[secondpart], Toast.LENGTH_SHORT).show();
                } else if (secondpart == 1) {
                    Toast.makeText(MainActivity.this, hundreds[firstpart] + tentonineteen[thirdpart], Toast.LENGTH_SHORT).show();
                } else if (secondpart == 0 && thirdpart != 0) {
                    Toast.makeText(MainActivity.this, hundreds[firstpart] + unity[thirdpart], Toast.LENGTH_SHORT).show();
                } else if (secondpart == 0 && thirdpart == 0 && firstpart != 0) {
                    Toast.makeText(MainActivity.this, hundreds[firstpart], Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, hundreds[firstpart] + tens[secondpart] + unity[thirdpart], Toast.LENGTH_SHORT).show();
                }*/

            }
        });

    }
}